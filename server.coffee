coffeeScript = require('coffee-script/register')
express = require('express')
app = express()
cookieParser = require('cookie-parser')
winston = require('winston')
expressWinston = require('express-winston')

app.use cookieParser()

app.set (expressWinston.logger)
  transports: [new (winston.transports.Console)({json: true,colorize: true})]
  meta: false
  msg: 'HTTP {{req.method}} {{req.url}}'
  expressFormat: true
  colorize: true

app.set 'views', __dirname + '/public/views'
app.set 'view engine', 'jade'
app.use express.static(__dirname + '/public')

app.get '/', (req, res) ->

  myContacts = req.cookies.myContacts

  if myContacts
    contacts = JSON.parse(myContacts)
  else
    contacts = []
  
  res.render 'index',
    contacts: contacts
    title: 'PhoneBook'

app.get '/partials/contactList', (req, res) ->
  res.render 'contactList'

app.listen 3000
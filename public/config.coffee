phonebook.config [
  '$stateProvider'
  ($stateProvider) ->
    $stateProvider
    .state('list',
      url: '/'
      templateUrl: '/partials/contactList'
      controller: 'ContactsController'
    )
    .state('edit-contact',
      parent: 'list'
      url: 'edit-contact'
      onEnter: [
        '$uibModal'
        '$state'
        ($uibModal, $state) ->
          $uibModal.open(
            templateUrl: '/views/contactEdit.html'
            controller: 'ContactsController'
            windowClass: 'right fade').result.finally ->
            $state.go '^'
    ])
    .state('view-contact',
      parent: 'list'
      url: 'view-contact'
      onEnter: [
        '$uibModal'
        '$state'
        ($uibModal, $state) ->
          $uibModal.open(
            templateUrl: '/views/contactView.html'
            controller: 'ContactsController'
            windowClass: 'right fade').result.finally ->
            $state.go '^'
    ])
    .state('add-contact',
      parent: 'list'
      url: 'add-contact'
      onEnter: [
        '$uibModal'
        '$state'
        ($uibModal, $state) ->
          $uibModal.open(
            templateUrl: '/views/contactAdd.html'
            controller: 'ContactsController'
            windowClass: 'right fade').result.finally ->
            $state.go '^'
    ])
    .state('remove-contact',
      parent: 'list'
      url: 'remove-contact'
      onEnter: [
        '$uibModal'
        '$state'
        ($uibModal, $state) ->
          $uibModal.open(
            templateUrl: '/views/contactRemove.html'
            controller: 'ContactsController'
            windowClass: 'right fade').result.finally ->
            $state.go '^'
    ])
]


phonebook.config ['$locationProvider',

  ($locationProvider) -> $locationProvider.html5Mode(true)

]
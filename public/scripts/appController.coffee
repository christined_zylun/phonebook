phonebook.controller 'ContactsController', ['$scope', '$window', '$cookies', ($scope, $window, $cookies) ->

  expireDate = new Date
  expireDate.setDate expireDate.getDate() + 1

  $scope.contacts = $window.contacts

  $scope.setSelected = (index) ->
    $window.selectedindex = index
    $window.selected = $scope.contacts[index]

  $scope.getContact = () ->
    $scope.selectedIndex = $window.selectedindex
    $scope.selected = angular.copy($window.selected)

  $scope.editContact = () ->
    $scope.contacts[$scope.selectedIndex] = $scope.selected
    $cookies.putObject 'myContacts', $scope.contacts, 'expires': expireDate

  $scope.removeContact = () ->
    $scope.contacts.splice($scope.selectedIndex, 1)
    $cookies.putObject 'myContacts', $scope.contacts, 'expires': expireDate

  $scope.addContact = () ->
    $scope.contacts.push($scope.selected)
    $cookies.putObject 'myContacts', $scope.contacts, 'expires': expireDate

]

phonebook.filter 'mob', ->
  (mob) ->
    if !mob
      return ''
    value = mob.toString().trim().replace(/^\+/, '')
    if value.match(/[^0-9]/)
      return mob
    number = value.slice(0, 4) + '-' + value.slice(4, value.length)
    number.trim()
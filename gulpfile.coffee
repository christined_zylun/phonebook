gulp = require('gulp')
coffee = require('gulp-coffee')
concat = require('gulp-concat')
minifyCSS = require('gulp-minify-css')
gulpIf = require('gulp-if')
gutil = require ('gulp-util')
ifElse = require('gulp-if-else')
uglify = require('gulp-uglify')
jade = require('gulp-jade')
replace = require('gulp-replace')
server = require('gulp-develop-server')
livereload = require('gulp-livereload')
argv = require('yargs')
stylus = require('gulp-stylus')

paths = 
  scripts: [ './public/**/*.coffee' ]
  templates: [ './public/views/*.jade' ]
  styles: [ './public/styles/*.styl' ]
  lib_scripts: ['./public/lib/angular/angular.min.js', './public/lib/angular-ui-router/release/angular-ui-router.min.js']

gulp.task 'templates', ->
  gulp.src(paths.templates).pipe(jade(
    jade: jade
    pretty: true)).pipe gulp.dest('public/views')

gulp.task 'scripts', ->
  gulp.src(paths.scripts)
  .pipe(coffee({bare: true}).on 'error', gutil.log)
  .pipe(gulpIf(argv.production, uglify()))
  .pipe(livereload(server))
  .pipe(concat('app.min.js'))
  .pipe gulp.dest('public/scripts')

gulp.task 'styles', ->
  gulp.src(paths.styles)
  .pipe(stylus())
  .pipe(minifyCSS())
  .pipe(ifElse(argv.production, minifyCSS, ->
    minifyCSS debug: true
  ))
  .pipe(livereload(server))
  .pipe(concat('app.min.css'))
  .pipe gulp.dest('public/styles')

gulp.task 'watch', ->
  gulp.watch paths.templates, [ 'templates' ]
  gulp.watch paths.scripts, [ 'scripts' ]
  gulp.watch paths.stles, [ 'styles' ]

gulp.task 'nodestart', ->
  server.listen { path: './server.coffee' }

gulp.task 'default', [
  'nodestart'
  'templates'
  'scripts'
  'styles'
  'watch'
]